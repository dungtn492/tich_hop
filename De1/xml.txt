<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" href="HoaDon.xslt"?>

<HD MaHD="hd01">
	<LoaiHang TenLoai="Rau Co">
		<MaLoai>DT</MaLoai>
		<Hang MaHang="H01">
			<TenHang>Ti vi</TenHang>
			<SoLuong>50</SoLuong>
			<DonViTinh>Chiec</DonViTinh>
			<DonGia>9500</DonGia>
		</Hang>

		<Hang MaHang="H02">
			<TenHang>Ti vi</TenHang>
			<DonViTinh>Chiec</DonViTinh>
			<DonGia>9500</DonGia>
		</Hang>

		<Hang MaHang="HD3">
			<TenHang>Ti vi</TenHang>
			<SoLuong>50</SoLuong>
			<DonViTinh>Chiec</DonViTinh>
			<DonGia>9500</DonGia>
		</Hang>
	</LoaiHang>

	<LoaiHang TenLoai="Thit">
		<MaLoai>DT</MaLoai>
		<Hang MaHang="H03">
			<TenHang>Ti vi</TenHang>
			<SoLuong>50</SoLuong>
			<DonViTinh>Chiec</DonViTinh>
			<DonGia>9500</DonGia>
		</Hang>

		<Hang MaHang="H04">
			<TenHang>Ti vi</TenHang>
			<DonViTinh>Chiec</DonViTinh>
			<DonGia>9500</DonGia>
		</Hang>

		<Hang MaHang="HD5">
			<TenHang>Ti vi</TenHang>
			<SoLuong>50</SoLuong>
			<DonViTinh>Chiec</DonViTinh>
			<DonGia>9500</DonGia>
		</Hang>
	</LoaiHang>
</HD>