<?xml version="1.0" encoding="utf-8" ?>
<?xml-stylesheet type="text/xsl" href="ThuVien.xslt"?>

<TV TenTV="Kim Dong">
	<NhaXB TenNXB="Giao Duc">
		<Sach MaSach="S01">
			<TenSach>Nha Gia Kim</TenSach>
			<TheLoai>Tieu Thuyet</TheLoai>
			<SoTrang>400</SoTrang>
		</Sach>
		<Sach MaSach="S02">
			<TenSach>Dac Nhan Tam</TenSach>
			<TheLoai>Tieu Thuyet</TheLoai>
			<SoTrang>50</SoTrang>
		</Sach>
		<Sach MaSach="S03">
			<TenSach>Tren Duong Bang</TenSach>
			<TheLoai>Tieu Thuyet</TheLoai>
			<SoTrang>200</SoTrang>
		</Sach>
	</NhaXB>

	<NhaXB TenNXB="Khoa Hoc Ky Thuat">
		<Sach MaSach="S04">
			<TenSach>C++</TenSach>
			<TheLoai>Hoc Thuat</TheLoai>
			<SoTrang>400</SoTrang>
		</Sach>
		<Sach MaSach="S05">
			<TenSach>Java</TenSach>
			<TheLoai>Hoc Thuat</TheLoai>
			<SoTrang>50</SoTrang>
		</Sach>
		<Sach MaSach="S06">
			<TenSach>C#</TenSach>
			<TheLoai>Hoc Thuat</TheLoai>
			<SoTrang>200</SoTrang>
		</Sach>
	</NhaXB>
</TV>