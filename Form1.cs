﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ComBo();
            HienThi();
        }

        public void HienThi()
        {
            string link = "http://localhost/vandungday/api/nhanvien";
            HttpWebRequest request = HttpWebRequest.CreateHttp(link);
            WebResponse response = request.GetResponse();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(NhanVien[]));
            object data = json.ReadObject(response.GetResponseStream());

            NhanVien[] arr = data as NhanVien[];

            datanhanvien.DataSource = arr;
        }

        public void ComBo()
        {
            string link = "http://localhost/vandungday/api/donvi";
            HttpWebRequest request = HttpWebRequest.CreateHttp(link);
            WebResponse response = request.GetResponse();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(DonVi[]));
            object data = json.ReadObject(response.GetResponseStream());

            DonVi[] arr = data as DonVi[];

            comboDonVi.DataSource = arr;
            comboDonVi.ValueMember = "MaDonVi";
            comboDonVi.DisplayMember = "TenDonVi";
        }

        private void datanhanvien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int d = e.RowIndex;
            txt_maNV.Text = datanhanvien.Rows[d].Cells[0].Value.ToString();
            txt_ten.Text = datanhanvien.Rows[d].Cells[1].Value.ToString();
            txt_gt.Text = datanhanvien.Rows[d].Cells[2].Value.ToString();
            txt_hsl.Text = datanhanvien.Rows[d].Cells[3].Value.ToString();
            string madv = datanhanvien.Rows[d].Cells[4].Value.ToString();

            comboDonVi.Text = comboDonVi.Items.Cast<DonVi>()
                .FirstOrDefault(x => x.MaDonVi.ToString() == madv).TenDonVi;

            //comboDonVi.Text = comboDonVi.Items.Cast<DonVi>().FirstOrDefault(x => x.MaDonVi.ToString() == madv).TenDonVi;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //create(int manv, string ten, string gt, int hsl, int madv)
            //?manv = 5 & ten = Nguyễn Minh & gt = Nữ & hsl = 5 & madv = 1
            string post = string.Format("?manv={0}&ten={1}&gt={2}&hsl={3}&madv={4}",
                txt_maNV.Text, txt_ten.Text, txt_gt.Text, txt_hsl.Text, comboDonVi.SelectedValue);

            string link = "http://localhost/vandungday/api/nhanvien" +  post;
            HttpWebRequest request = HttpWebRequest.CreateHttp(link);
            request.Method = "POST";
            request.ContentType = "application/json;charset=UTF-8";

            byte[] byteArray = Encoding.UTF8.GetBytes(post);
            request.ContentLength = byteArray.Length;

            Stream stream = request.GetRequestStream();
            stream.Write(byteArray, 0, byteArray.Length);
            stream.Close();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(bool));
            object data = json.ReadObject(request.GetResponse().GetResponseStream());

            bool kq = (bool)data;

            if(kq)
            {
                HienThi();
                MessageBox.Show("Them thanh cong");
            }else
            {
                MessageBox.Show("Them that bai");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           

            string link = "http://localhost/vandungday/api/nhanvien?manv=" + txt_maNV.Text;
            HttpWebRequest request = HttpWebRequest.CreateHttp(link);
            request.Method = "DELETE";
            request.ContentType = "application/json;charset=UTF-8";

            byte[] byteArray = Encoding.UTF8.GetBytes(txt_maNV.Text);
            request.ContentLength = byteArray.Length;

            Stream stream = request.GetRequestStream();
            stream.Write(byteArray, 0, byteArray.Length);
            stream.Close();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(bool));
            object data = json.ReadObject(request.GetResponse().GetResponseStream());

            bool kq = (bool)data;

            if (kq)
            {
                HienThi();
                MessageBox.Show("Xoa thanh cong");
            }
            else
            {
                MessageBox.Show("Xoa that bai");
            }
        }
    }
}
