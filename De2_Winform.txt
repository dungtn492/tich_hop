﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Net;

namespace De2_Winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            HienThiCombo();
            HienThiData();
        }

        private void HienThiData()
        {
            string link = "http://localhost/hocrestful/api/nhanvien";
            HttpWebRequest req = HttpWebRequest.CreateHttp(link);
            WebResponse res = req.GetResponse();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(NhanVien[]));
            object data = json.ReadObject(res.GetResponseStream());
            NhanVien[] arr = data as NhanVien[];
            datanhanvien.DataSource = arr;
        }

        private void HienThiCombo()
        {
            string link = "http://localhost/hocrestful/api/donvi";
            HttpWebRequest req = HttpWebRequest.CreateHttp(link);
            WebResponse res = req.GetResponse();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(DonVi[]));
            object data = json.ReadObject(res.GetResponseStream());
            DonVi[] arr = data as DonVi[];
            comboDV.DataSource = arr;
            comboDV.ValueMember = "MaDonVi";
            comboDV.DisplayMember = "TenDonVi";
        }
        private void datanhanvien_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            txt_maNV.Text = datanhanvien.Rows[index].Cells[0].Value.ToString();
            txt_tenNV.Text = datanhanvien.Rows[index].Cells[1].Value.ToString();
            txt_gioiTinh.Text = datanhanvien.Rows[index].Cells[2].Value.ToString();
            txt_hSL.Text = datanhanvien.Rows[index].Cells[3].Value.ToString();

            //string madv = datanhanvien.Rows[index].Cells[4].Value.ToString();
            //string link = "http://localhost/hocrestful/api/donvi?madv="+madv;
            //HttpWebRequest req = HttpWebRequest.CreateHttp(link);
            //WebResponse res = req.GetResponse();

            //DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(DonVi));
            //object data = json.ReadObject(res.GetResponseStream());
            comboDV.Text = datanhanvien.Rows[index].Cells[4].Value.ToString();

        }

        private void them_Click(object sender, EventArgs e)
        {
            string postString = string.Format("?manv={0}&ten={1}&gt={2}&hsl={3}&madv={4}",
                txt_maNV.Text, txt_tenNV.Text, txt_gioiTinh.Text, txt_hSL.Text, comboDV.SelectedValue);

            string link = "http://localhost/hocrestful/api/nhanvien/" + postString;
            HttpWebRequest request = HttpWebRequest.CreateHttp(link);
            request.Method = "POST";
            request.ContentType = "application/json;charset=UTF-8";

            byte[] byteArr = Encoding.UTF8.GetBytes(postString);
            request.ContentLength = byteArr.Length;

            Stream stream = request.GetRequestStream();
            stream.Write(byteArr, 0, byteArr.Length);
            stream.Close();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(bool));
            object data = json.ReadObject(request.GetResponse().GetResponseStream());
            bool kq = (bool)data;

            if (kq)
            {
                HienThiData();
                MessageBox.Show("Thêm thành công");
            }
            else
                MessageBox.Show("Thêm thất bại");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string postString = string.Format("?manv={0}&ten={1}&gt={2}&hsl={3}&madv={4}",
               txt_maNV.Text, txt_tenNV.Text, txt_gioiTinh.Text, txt_hSL.Text, comboDV.SelectedValue);

            string link = "http://localhost/hocrestful/api/nhanvien/" + postString;

            HttpWebRequest request = HttpWebRequest.CreateHttp(link);
            request.Method = "PUT";
            request.ContentType = "application/json;charset=UTF-8";

            byte[] byteArray = Encoding.UTF8.GetBytes(postString);
            request.ContentLength = byteArray.Length;

            Stream stream = request.GetRequestStream();
            stream.Write(byteArray, 0, byteArray.Length);
            stream.Close();

            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(bool));
            object data = json.ReadObject(request.GetResponse().GetResponseStream());
            bool kq = (bool)data;
            if(kq)
            {
                HienThiData();
                MessageBox.Show("Sua ok");
            }else
            {
                MessageBox.Show("Sua faile");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

      
    }
}
